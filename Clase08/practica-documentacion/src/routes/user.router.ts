import { Router } from "express";
import {getUsers, getUser, createUser, updateUser, deleteUser} from "../controllers/user.controller";
import {getEvents} from "../controllers/event.controller";
import {signIn, signUp, protectedEndpoint, refresh } from '../controllers/user.controller'
import passport from 'passport'

const router = Router();

/**
 * @swagger
 * components:
 *  schemas:
 *    User:
 *      type: object
 *      properties:
 *        id:
 *          type: number
 *          description: the auto-generated id of task
 *        email:
 *          type: string
 *          description: the email of the user
 *        password:
 *          type: string
 *          description: the password of the user
 *        active:
 *          type: boolean
 *          description: is active for the user
 *        createdAt:
 *          type: string
 *          description: date created
 *        updatedAt:
 *          type: string
 *          description: date updated
 *      required:
 *        - email
 *        - password
 *      example:
 *        id: gQBOyGbxcQy6tEp0aZ78X
 *        email: email
 *        password: password
 *  parameters:
 *      taskId:
 *          in: path
 *          name: id
 *          required: true
 *          schema:
 *              type: string
 *          description: the task id
 */


/**
 * @swagger
 * tags:
 *  name: Users
 *  description: Users endpoint
 */

/**
 * @swagger
 * /api/users:
 *  get:
 *    summary: Returns a list of users
 *    tags: [Users]
 *    responses:
 *      200:
 *        description: the list of users
 *        content:
 *          application/json:
 *            schema:
 *              type: array
 *              items:
 *                  $ref: '#/components/schemas/User'
 *                  
 */

/**
 * @swagger
 * /api/users:
 *  post:
 *    summary: Create a user
 *    tags: [Users]
 *    responses:
 *      200:
 *        description: the  user
 *        content:
 *          application/json:
 *            schema:
 *              type: array
 */

router.get("/", getUsers);
router.get("/:id", getUser);
router.post("/", createUser);
router.put("/:id", updateUser);
router.delete("/:id", deleteUser);


//Agregar para jwt
router.post('/signup', signUp);
router.post('/signin', signIn);
router.post('/token', refresh);
router.post('/protected', passport.authenticate('jwt', { session: false }), protectedEndpoint);

export default router;