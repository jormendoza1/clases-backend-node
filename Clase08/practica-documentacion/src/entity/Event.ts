import {Entity,Column, PrimaryGeneratedColumn,BaseEntity,CreateDateColumn,UpdateDateColumn} from "typeorm";
  
  @Entity() // se puede pasar como parametro el nombre de tabla ej: 'usersTable'

  export class Event extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    type: string;
  
    @Column()
    name: string;
  
    @Column()
    dateEvent: Date;

    @Column()
    gps: string;
  
    @CreateDateColumn()
    createdAt: Date;
  
    @UpdateDateColumn()
    updatedAt: Date;
  }