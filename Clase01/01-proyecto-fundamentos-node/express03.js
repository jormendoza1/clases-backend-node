/**
 * REST API, POST con status 200 y 400
 */
 
const express = require('express');
const app = express();
const bodyParser = require('body-parser');
app.use(bodyParser.json());



app.post('/usuario', (req, res) => {
    const { email } = req.body;
    if (!email) {
        res.status(400).json({
            ok: false,
            mensaje: 'El email es necesario'
        });
    } else {
        res.status(200).json({
            usuario: email
        });
    }
});


app.get('/empleado', (req, res) => {
    res.json('GET Empleado');
});

app.post('/empleado', (req, res) => {
    const { nombre } = req.body;
    res.status(200).json({
        empleado: nombre
    });
});

app.put('/empleado/:id', (req, res) => {
    const id = req.params.id;
    res.json({
        id: id
    });
});

app.delete('/empleado', (req, res) => {
    res.json('DELETE Empleado');
});

app.listen(3000);