/**
 * GET with params
 */

const express = require('express')
const app = express()

app.get('/', (req, res) => {
    const user = {
        name: 'Jorge',
        lastName: 'Mendoza'
    }
    res.send(user)
})

app.get('/empleados/:id', (req, res) => {    
    res.send({
        id: req.params.id
    })
})

app.listen(3000)