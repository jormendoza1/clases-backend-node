
const pi = 3.14;
const area = (width) => {
    return width * width;
}
const perimeter = (width) => {
    return 4 * width;
}

module.exports = {
    pi,
    area,
    perimeter    
};