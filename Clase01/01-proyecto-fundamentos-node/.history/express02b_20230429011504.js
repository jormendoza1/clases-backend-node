/**
 * GET with params
 */

const express = require('express')
const app = express()

app.get('/empleados/:id', (req, res) => {    
    res.send({
        id: req.params.id
    })
})

app.get('/empleados/:section/:id', (req, res) => {    
    res.send({
        id: req.params.id,
        section: req.params.section
    })
})

// http://localhost:3000/sector/12345
app.get('/sector/:id([0-9]{5})', (req, res) => {    
    res.send({
        id: req.params.id
    })
})

app.listen(3000);