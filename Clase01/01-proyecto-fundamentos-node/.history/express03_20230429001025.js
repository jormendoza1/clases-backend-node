/**
 * POST con status 200 y 400
 */
 
const express = require('express');
const app = express();
const bodyParser = require('body-parser');
app.use(bodyParser.json());


app.get('/empleado', function(req, res) {
    res.json('GET Empleado');
});

app.post('/empleado', function(req, res) {
    const { nombre } = req.body;
    if (!nombre) {
        res.status(400).json({
            ok: false,
            mensaje: 'El nombre es necesario'
        });
    } else {
        res.status(200).json({
            empleado: nombre
        });
    }
});

app.put('/empleado/:id', function(req, res) {
    const id = req.params.id;
    res.json({
        id: id
    });
});

app.delete('/empleado', function(req, res) {
    res.json('DELETE Empleado');
});

app.listen(3000);