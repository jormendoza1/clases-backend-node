// MODULES
const {area, perimeter, pi} = require('./utils/math')

console.log('Area: ', area(20));
console.log('Perimeter: ', perimeter(15));
console.log('PI: ', pi);



// FUNCTIONS
const hello = (name) => {
    return 'Hi ' + name;
}

function hello2 (name) {
    return 'Hi ' + name;
}


console.log(hello('Jorge1'));
console.log(hello2('Jorge2'));