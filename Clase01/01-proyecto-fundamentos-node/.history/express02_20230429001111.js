/**
 * GET con retornando JSON
 */

const express = require('express')
const app = express()

app.get('/', (req, res) => {
    const user = {
        name: 'Jorge',
        lastName: 'Mendoza'
    }
    res.send(user)
})

app.get('/saludo', (req, res) => {
    res.send('Hola Mundo Express')
})

app.listen(3000)