const hello = (name) => {
    return 'Hi ' + name;
}

function hello2 (name) {
    return 'Hi ' + name;
}

console.log(hello('Jorge'));
console.log(hello2('Jorge'));