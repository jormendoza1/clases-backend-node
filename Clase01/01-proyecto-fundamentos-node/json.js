// Destructure

  const userDestructure = {
    name: "Manz",
    role: "streamer",
    life: 99
  }
  const { name, role, life } = userDestructure;
  console.log(name);
  console.log(role, life);
  

  // Destructuracion REST
  
  const userRest = {
    nameRest: "Manz",
    role: "streamer",
    life: 99
  }
  const { nameRest, ...rest } = userRest;
  console.log(rest);


  // Restructurar Objetos
  const user = {
    name: "Manz",
    role: "streamer",
    life: 99
  }
  
  const fullUser = {
    ...user,
    power: 25,
    life: 50
  }




// JSON.stringify
  const json = `{
    "name": "Manz",
    "life": 99
  }`;  
  const user1 = JSON.parse(json);
  console.log(user1);
  
// JSON.parse
  const user2 = {
    name: "Manz",
    life: 99,
    talk: function () {
      return "Hola!";
    },
  };  
  console.log(JSON.stringify(user2)); 

