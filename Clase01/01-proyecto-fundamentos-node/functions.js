// MODULES
const {area, perimeter, pi} = require('./utils/math')

console.log('Area: ', area(20));
console.log('Perimeter: ', perimeter(15));
console.log('PI: ', pi);



// FUNCTIONS
const hello = (name) => {
    return 'Hi ' + name;
}

function hello2 (name) {
    return 'Hi ' + name;
}


console.log(hello('Jorge1'));
console.log(hello2('Jorge2'));




/* let meses = ["enero", "febrero", "lunes", "martes"];
let dias = meses.splice(2, 1);

console.log(dias); // ["lunes"]
console.log(meses); // ["enero", "febrero", "martes"] */