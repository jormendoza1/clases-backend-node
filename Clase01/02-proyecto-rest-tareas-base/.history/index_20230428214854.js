const express = require("express");
const app = express();
const bodyParser = require('body-parser');
app.use(bodyParser.json());
const port = 3000;


app.get("/tareas", (req, res) => {
    res.json({
        tareas
    });
});

app.get("/tareas/:id", (req, res) => {
    res.json({ 
        id: req.params.id
    });
});

app.put("/tareas/:id", (request, response) => {
    res.json({ 
        id: req.params.id
    });
});

app.post("/tareas", (req,res) => {
    tareas.push(req.body);
    res.json({
        tareas
    });
});

app.delete("/tareas/:id", (request, response) => {
    response.json({         
        id: request.params.id
    });
});



const tareas = [
    {
        id: 1,
        title: "tarea 1",
        completed: false
    },
    {
        id:2,
        title: "tarea 2",
        completed: false
    },
    {
        id:3,
        title: "tarea 3",
        completed: false
    }
];


app.listen(port,() => {
    console.log('Iniciando aplicacion node en puerto: ' + port);
});