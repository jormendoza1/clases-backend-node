const express = require("express");
const app = express();
const bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
const port = 3000;


app.get("/tareas", (req, res) => {
    res.status(200).json({
        tareas
    });
});

app.get("/tareas/:id", (req, res) => {
    res.status(200).json({ 
        data: tareas.find((tarea) => tarea.id === req.params.id) 
    });
});

app.put("/tareas/:id", (request, response) => {
    const tarea = tareas.find((tarea) => tarea.id === request.params.id);
    if (tarea) {
      const { title, completed } = request.body;
      tarea.title = title;      
      tarea.completed = completed;
      response.status(200).json({ 
        msg: "Tarea actualizada exitosamente" 
      });
      return;
    }
    response.status(404).json({ 
        msg: "Tarea no encontrada" 
    });
  });

app.post("/tareas", (req,res) => {
    tareas.push(req.body);
    res.status.json({
        tareas
    });
});

app.delete("/tareas/:id", (request, response) => {
    const tareaIndex = tareas.findIndex((tarea) => (tarea.id = request.params.id));
    if (tareaIndex) {
      tareas.splice(tareaIndex, 1);
      response.status(200).json({ 
        msg: "Todo deleted successfully" 
      });
    }
    response.status(404).json({ 
        msg: "Todo not found" 
    });
});




const tareas = [
    {
        title: "tarea 1",
        completed: false
    },
    {
        title: "tarea 2",
        completed: false
    },
    {
        title: "tarea 3",
        completed: false
    }
];


app.listen(port,() => {
    console.log('Iniciando aplicacion node en puerto: ' + port);
});