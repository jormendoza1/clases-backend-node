const express = require("express");
const app = express();
const port = 3000;


app.get("/", (request, response) => {
    let salida = {
        nombre: 'Jorge',
        apellido: 'Mendoza',
        url: request.url
    }
    response.send(salida)
});


app.listen(port,() => {
    console.log('Iniciando aplicacion node en puerto: ' + port);
});