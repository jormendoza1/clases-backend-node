const express = require("express");
const app = express();
const bodyParser = require('body-parser');
// app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
const port = 3000;


app.get("/tareas", (req, res) => {
    res.status(200).json({
        tareas
    });
});

app.get("/tareas/:id", (req, res) => {
    console.log(req.params.id);
    const found = tareas.find((tarea) => tarea.id === req.params.id);
    console.log(tareas);
    res.status(200).json({ 
        data: tareas.find((tarea) => tarea.id === req.params.id) 
    });
});

app.put("/tareas/:id", (request, response) => {
    const tarea = tareas.find((tarea) => tarea.id === request.params.id);
    if (tarea) {
      const { title, completed } = request.body;
      tarea.title = title;      
      tarea.completed = completed;
      response.status(200).json({ 
        msg: "Tarea actualizada exitosamente",
        tareas
      });
      return;
    }
    response.status(404).json({ 
        msg: "Tarea no encontrada" 
    });
  });

app.post("/tareas", (req,res) => {
    tareas.push(req.body);
    res.status.json({
        msg: "Tarea agregada exitosamente",
        tareas
    });
});

app.delete("/tareas/:id", (request, response) => {
    const tareaIndex = tareas.findIndex((tarea) => (tarea.id = request.params.id));
    if (tareaIndex) {
      tareas.splice(tareaIndex, 1);
      response.status(200).json({ 
        msg: "Tarea eliminada correctamente",
        tareas
      });
    }
    response.status(404).json({ 
        msg: "Tarea no encontrada" 
    });
});




const tareas = [
    {
        id: 1,
        title: "tarea 1",
        completed: false
    },
    {
        id:2,
        title: "tarea 2",
        completed: false
    },
    {
        id:3,
        title: "tarea 3",
        completed: false
    }
];


app.listen(port,() => {
    console.log('Iniciando aplicacion node en puerto: ' + port);
});