const express = require("express");
//bodyParser nos permite reicibir parametros por POST
const bodyParser = require('body-parser');

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));

// parse application/json
app.use(bodyParser.json());

const app = express();
const port = 3000;


app.get("/tareas", (req, res) => {
    res.json(tareas);
});


app.post("/tareas", (req,res) => {
    console.log(req);
    tareas.push(req.body);
    res.json({
        tareas
    });
});


const tareas = [
    {
        title: "tarea 1",
        completed: false
    },
    {
        title: "tarea 2",
        completed: false
    },
    {
        title: "tarea 3",
        completed: false
    }
];


app.listen(port,() => {
    console.log('Iniciando aplicacion node en puerto: ' + port);
});