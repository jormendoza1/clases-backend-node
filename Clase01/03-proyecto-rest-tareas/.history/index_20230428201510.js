const express = require("express");
const app = express();
const port = 3000;


app.get("/tareas", (req, res) => {
    res.json(tareas);
});


app.post("/tareas", (req,res) => {
    tareas.push(req.body);
    res.json({
        tareas
    })
})

const tareas = [
    {
        title: "tarea 1",
        completed: false
    },
    {
        title: "tarea 2",
        completed: false
    },
    {
        title: "tarea 3",
        completed: false
    }
];


app.listen(port,() => {
    console.log('Iniciando aplicacion node en puerto: ' + port);
});