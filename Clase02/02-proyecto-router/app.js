const express = require('express');
const app = express();
const port = 3000;
app.use(express.json());



//Rutas
app.use('/users', require('./routes/users.router'));
app.use('/employees', require('./routes/employees.router'));




app.listen(port, () => {
    console.log('servidor corriendo en el puerto', port)
})