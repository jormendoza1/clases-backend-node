const express = require('express');
const routerUsers = express.Router();


routerUsers.get('/', (req,res) => {
    res.send({
        saludo: 'Hola Users'
    });
});

routerUsers.get('/:id', (req,res) => {
    res.send({
        id: req.params.id
    });
});

routerUsers.post('/', (req, res) => {
    res.status(200).json({
        resp: req.body
    });
});


module.exports = routerUsers;