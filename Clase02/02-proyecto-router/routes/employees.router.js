const express = require('express');
const { v4: uuidv4 } = require('uuid');
const routerEmployees = express.Router();

routerEmployees.get('/', (req,res) => {
    res.send({
        saludo: 'Hola Employees',
    });
});

routerEmployees.get('/:id', (req,res) => {
    res.send({
        id: req.params.id
    });
});

routerEmployees.get('/code/uuid22', (req,res) => {
    let valor = uuidv4();
    res.send({
        id: valor
    });
});
module.exports = routerEmployees;