const { empleadosDB } = require ('./utils/db.js')
const express = require('express');
const app = express();
const port = 3000;

app.use(express.json());
app.use(express.urlencoded({ extended: true }));


app.get('/empleados', (req, res) => {
    res.send({
        empleadosDB
    });
});

app.get('/users', (req, res) => {
    const user_id = req.query.id;
    const token = req.query.token;
  
    res.send({
      'user_id': user_id,
      'token': token
    });
});

app.get('/empleados/:id', (req, res) => {
    res.send({
        empleado: empleadosDB.find( e => e.id == req.params.id)
    });
});


app.post('/empleados', (req, res) => {
    const { id, nombre, dni } = req.body;    

    empleadosDB.push({
        id,
        nombre,
        dni
    });

    res.status(200).json({
        empleadosDB
    });
});


app.put('/empleados/:id', (req, res) => {
    const id = req.params.id;    
    
    res.status(200).json({
        id,
        body: req.body
    });
});

app.delete('/empleados/:id', (req, res) => {
    const tareaIndex = empleadosDB.findIndex((e) => (e.id == request.params.id));
    if (empleadoIndex) {
        empleadosDB.splice(empleadoIndex, 1);
      response.status(200).json({ 
        msg: "Empleado eliminado correctamente",
        empleadosDB
      });
      return;
    }
    response.status(404).json({ 
        msg: "Empleado no encontrado" 
    });
})




app.listen(port,() => {
    console.log('Iniciando aplicacion node en puerto: ' + port);
});