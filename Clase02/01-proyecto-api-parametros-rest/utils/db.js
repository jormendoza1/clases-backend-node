const empleadosDB = [
    {
        id: 1,
        nombre: "Juan Mendez",
        dni: "111"
    },
    {
        id:2,
        nombre: "Juan Albornoz",
        dni: "222"
    },
    {
        id:3,
        nombre: "Juan Zalasar",
        dni: "333"
    }
];

module.exports = {
    empleadosDB
}