import { Router } from "express";
import {getUsers, getUser, createUser, updateUser, deleteUser} from "../controllers/user.controller";
import {getEvents} from "../controllers/event.controller";


const router = Router();



router.get("/", getEvents);
router.get("/:id", getUser);
router.post("/", createUser);
router.put("/:id", updateUser);
router.delete("/:id", deleteUser);



export default router;