const router = require('express').Router();
const {middleware1, middleware2} = require ('../middleware/middlewares.js');


/* Routes sin middlewares */
router.get('/' , (req , res)=>{
    // router code here
    res.send({ name: 'jorge'});
})
router.post('/' , (req , res)=>{
    // router code here
    res.statusCode(200).send({ ...req.body, ok: true});
})



/*  Routes con middlewares - a nivel de endpoint */
router.get('/ruta1', middleware1, (req, res) => {
    res.json({
        dato1: req.dato1
    })
 })
router.get('/ruta2', middleware1, middleware2, (req, res) => {
    res.json({
        dato1: req.dato1,
        dato2: req.dato2
    })
 })


 

module.exports  = router