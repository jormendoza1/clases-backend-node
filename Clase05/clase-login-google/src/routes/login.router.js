const router = require('express').Router();
const session = require('express-session');
const passport = require('passport');
require('../middleware/google');



/* Middleware que verifica que exista el user de google, si es asi, es un login correcto
* sino existe el user se manda un 401 */
  const isLoggedIn = (req, res, next) => {
    req.user ? next() : res.sendStatus(401);
  }
  

  router.use(session({ secret: 'cats', resave: false, saveUninitialized: true }));
  router.use(passport.initialize());
  router.use(passport.session());

  /* Muestra link para autenticacion google */
  router.get('/', (req, res) => {
    res.send('<a href="/auth/google/login">Authenticate with Google</a>');
  });
  
  /* Setea el scope del login, establece que se solicitara informacion de email y profile */
  router.get('/google/login', passport.authenticate('google', { scope: [ 'email', 'profile' ] }));


  /* Este es el ** callback ** configurado en el panel de google cloud. Al terminar el login, el resultado
* llega a este endpoint, el cual configura la redireccion con passport en caso de que sea success redirige al endpoint /protected
* si es failure redirige a /auth/google/failure */
router.get("/google", passport.authenticate("google", {
    successRedirect: "/auth/protected",
    failureRedirect: "/auth/google/failure",
  })
);


/* Endpoint protegido, requiere login correcto
 * usa el middleware isLoggedIn para verificar si existe el usuario de google */
router.get("/protected", checkReq, isLoggedIn, (req, res) => {
    console.log(req.user);
    res.send(`Hello ${req.user.displayName}`);
});

/* Endpoint para fallos, en caso de login incorrecto */
router.get("/google/failure", (req, res) => {
  res.send("Failed to authenticate..");
});

/* Logout, termina la sesion mediante la funcion logout() de passport
* destroy() destruye la session mediante passport */
router.get("/logout", (req, res) => {
  req.logout();
  req.session.destroy();
  res.send("Goodbye!");
});

module.exports  = router