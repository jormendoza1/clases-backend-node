// app.js
const config = require('../config/config');
const express = require('express');
const app = express();

// Rutas
app.use('/users', require('./routes/users.router'));


app.listen(config.PORT, config.HOST, () => {
   console.log(`NODE_ENV=${config.NODE_ENV}`);
   console.log(`App listening on http://${config.HOST}:${config.PORT}`);
});